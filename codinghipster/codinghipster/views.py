from resume.serializers import ResumeSerializer
from resume.models import Resume

from mezzanine.utils.views import render


def projects(request, **kwargs):
    return render(request, 'projects.html')


def _resume(request, **kwargs):
    resume = Resume.objects.last()
    context = ResumeSerializer(resume).data
    context['params'] = kwargs

    context['supported_formats'] = ('pdf', 'json', 'csv', 'xml', 'yaml', 'html')
    for (key, value) in context.items():
        if callable(value):
            context[key] = value()
    return context


def resume(request, **kwargs):
    return render(request, 'main.html', _resume(request, **kwargs))


def resume_standalone(request, **kwargs):
    return render(request, 'resume_standalone.html', _resume(request,
                                                             **kwargs))
