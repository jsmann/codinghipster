from rest_framework import serializers
from models import (
    Resume, Info, Interest, Job, Responsibility, Achievement, Skillset, Skill,
    Education
)


class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info
        fields = (
            'name', 'email', 'address', 'city', 'state', 'zipcode',
            'phone_number'
        )


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = ('description',)


class ResponsibilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Responsibility
        fields = ('description',)


class AchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Achievement
        fields = ('description',)


class JobSerializer(serializers.ModelSerializer):
    responsibilities = ResponsibilitySerializer(many=True)
    achievements = AchievementSerializer(many=True)
    start_date = serializers.ReadOnlyField()
    end_date = serializers.ReadOnlyField()

    class Meta:
        model = Job
        fields = (
            'title', 'company', 'location', 'start_date', 'end_date',
            'summary', 'responsibilities', 'achievements'
        )


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ('value',)


class SkillsetSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(many=True)

    class Meta:
        model = Skillset
        fields = ('name', 'skills')


class EducationSerializer(serializers.ModelSerializer):
    start_date = serializers.ReadOnlyField()
    end_date = serializers.ReadOnlyField()

    class Meta:
        model = Education
        fields = (
            'school', 'location', 'start_date', 'end_date', 'degree',
            'concentration'
        )


class ResumeSerializer(serializers.ModelSerializer):
    info = InfoSerializer()
    interests = InterestSerializer(many=True)
    employment = JobSerializer(many=True)
    skillset = SkillsetSerializer(many=True)
    education = EducationSerializer(many=True)
    last_modified = serializers.DateTimeField(
        source='get_last_modified', read_only=True
    )

    class Meta:
        model = Resume
        fields = (
            'info', 'interests', 'employment', 'skillset', 'education',
            'last_modified'
        )
