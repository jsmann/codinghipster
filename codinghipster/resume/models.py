import uuid

from django.db import models


class LastModifiedMixin(models.Model):
    last_modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Cache(LastModifiedMixin):
    data = models.BinaryField()


class Resume(LastModifiedMixin):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def get_last_modified(self):
        items = [
            self.info,
            self.interests.latest(field_name='last_modified'),
            self.employment.latest(field_name='last_modified'),
            self.skillset.latest(field_name='last_modified')
        ]
        try:
            items.extend([
                job.responsibilities.latest(field_name='last_modified')
                for job in self.employment.iterator()
        ])
        except Responsibility.DoesNotExist:
            pass
        try:
            items.extend([
                job.achievements.latest(field_name='last_modified')
                for job in self.employment.iterator()
            ])
        except Achievement.DoesNotExist:
            pass
        items.extend([
            skillset.skills.latest(field_name='last_modified')
            for skillset in self.skillset.iterator()
        ])
        return max(items, key=lambda value: value.last_modified).last_modified


class Info(LastModifiedMixin):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=2)
    zipcode = models.CharField(max_length=5)
    phone_number = models.CharField(max_length=20)
    resume = models.OneToOneField(Resume, related_name="info")


class Interest(LastModifiedMixin):
    importance = models.IntegerField()
    description = models.TextField()
    resume = models.ForeignKey(Resume, related_name="interests")

    class Meta:
        ordering = ('importance',)


class Job(LastModifiedMixin):
    title = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    summary = models.TextField()
    resume = models.ForeignKey(Resume, related_name="employment")

    class Meta:
        ordering = ('-start_date', '-end_date')


class Responsibility(LastModifiedMixin):
    importance = models.IntegerField()
    description = models.TextField()
    job = models.ForeignKey(Job, related_name="responsibilities")

    class Meta:
        ordering = ('importance',)


class Achievement(LastModifiedMixin):
    importance = models.IntegerField()
    description = models.TextField()
    job = models.ForeignKey(Job, related_name="achievements")

    class Meta:
        ordering = ('importance',)


class Skillset(LastModifiedMixin):
    name = models.CharField(max_length=100)
    resume = models.ForeignKey(Resume, related_name="skillset")


class Skill(LastModifiedMixin):
    importance = models.IntegerField()
    value = models.CharField(max_length=50)
    skillset = models.ForeignKey(Skillset, related_name="skills")

    class Meta:
        ordering = ('importance',)


class Education(LastModifiedMixin):
    school = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField(null=True)
    degree = models.CharField(max_length=50)
    concentration = models.CharField(max_length=50)
    resume = models.ForeignKey(Resume, related_name="education")

    class Meta:
        ordering = ('-end_date', '-start_date')
