# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('school', models.CharField(max_length=100)),
                ('location', models.CharField(max_length=100)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(null=True)),
                ('degree', models.CharField(max_length=50)),
                ('concentration', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ('-end_date', '-start_date'),
            },
        ),
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=100)),
                ('address', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=50)),
                ('state', models.CharField(max_length=2)),
                ('zipcode', models.CharField(max_length=5)),
                ('phone_number', models.CharField(max_length=20)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('importance', models.IntegerField()),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ('importance',),
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('title', models.CharField(max_length=100)),
                ('company', models.CharField(max_length=100)),
                ('location', models.CharField(max_length=100)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(null=True, blank=True)),
                ('summary', models.TextField()),
            ],
            options={
                'ordering': ('-start_date', '-end_date'),
            },
        ),
        migrations.CreateModel(
            name='Responsibility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('importance', models.IntegerField()),
                ('description', models.TextField()),
                ('job', models.ForeignKey(related_name='responsibilities', to='resume.Job')),
            ],
            options={
                'ordering': ('importance',),
            },
        ),
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('importance', models.IntegerField()),
                ('value', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ('importance',),
            },
        ),
        migrations.CreateModel(
            name='Skillset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now_add=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('resume', models.ForeignKey(related_name='skillset', to='resume.Resume')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='skill',
            name='skillset',
            field=models.ForeignKey(related_name='skills', to='resume.Skillset'),
        ),
        migrations.AddField(
            model_name='job',
            name='resume',
            field=models.ForeignKey(related_name='employment', to='resume.Resume'),
        ),
        migrations.AddField(
            model_name='interest',
            name='resume',
            field=models.ForeignKey(related_name='interests', to='resume.Resume'),
        ),
        migrations.AddField(
            model_name='info',
            name='resume',
            field=models.OneToOneField(related_name='info', to='resume.Resume'),
        ),
        migrations.AddField(
            model_name='education',
            name='resume',
            field=models.ForeignKey(related_name='education', to='resume.Resume'),
        ),
    ]
