# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0003_auto_20160129_1546'),
    ]

    operations = [
        migrations.CreateModel(
            name='Achievement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('importance', models.IntegerField()),
                ('description', models.TextField()),
                ('job', models.ForeignKey(related_name='achievements', to='resume.Job')),
            ],
            options={
                'ordering': ('importance',),
            },
        ),
    ]
