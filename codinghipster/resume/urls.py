from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        r'^1?/?$', views.ResumeView.as_view(),
        {
            'template_name': 'resume_api.html',
            'css': 'style.css'
        }
    ),
]
