import dateutil.parser
import os

from django.template import loader, TemplateDoesNotExist, RequestContext
from django.template.backends.django import Template

from rest_framework.renderers import BaseRenderer
from rest_framework.exceptions import NotAcceptable

import pdfkit

from codinghipster import settings
from models import Cache


def __generate_pdf(html, options):
    data = Cache()
    data.data = pdfkit.from_string(html, False, options=options)
    data.save()
    return data


def cache(html, options, last_modified):
    data = None
    try:
        data = Cache.objects.get()
    except Cache.DoesNotExist:
        data = __generate_pdf(html, options)
        return data.data

    if data.last_modified < dateutil.parser.parse(last_modified):
        data.delete()
        data = __generate_pdf(html, options)
        return data.data
    return data.data


class HTMLRenderer(BaseRenderer):
    media_type = 'text/html'
    format = 'html'
    charset = 'utf-8'

    def get_html(self, data, media_type=None, renderer_context=None):
        html_template = None
        try:
            html_template = renderer_context['kwargs']['template_name']
        except AttributeError:
            raise NotAcceptable("no template found")

        if type(html_template) != Template:
            try:
                html_template = loader.get_template(html_template)
            except TemplateDoesNotExist:
                raise NotAcceptable("no template found")

        context = RequestContext(renderer_context['request'], data)
        return html_template.render(context)

    def render(self, data, media_type=None, renderer_context=None):
        return self.get_html(data, media_type, renderer_context)


class PDFRenderer(HTMLRenderer):
    media_type = 'application/pdf'
    format = 'pdf'
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        html = self.get_html(data, media_type, renderer_context)

        options = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            'quiet': ''
        }
        return cache(html, None, options, data.get('last_modified'))

