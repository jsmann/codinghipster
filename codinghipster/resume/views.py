from models import Resume

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework_xml.renderers import XMLRenderer
from rest_framework_csv.renderers import CSVRenderer
from rest_framework_yaml.renderers import YAMLRenderer

from serializers import ResumeSerializer
from renderers import HTMLRenderer, PDFRenderer
from codinghipster import settings


class ResumeView(APIView):
    renderer_classes = ([BrowsableAPIRenderer] if settings.DEBUG else []) +\
            [
                JSONRenderer, HTMLRenderer, XMLRenderer, YAMLRenderer,
                CSVRenderer, PDFRenderer
            ]
    filename = 'resume-jmann'

    def get(self, request, *args, **kwargs):
        resume = Resume.objects.last()
        serializer = ResumeSerializer(resume)
        return Response(serializer.data)

    def finalize_response(self, request, response, *args, **kwargs):
        response = super(ResumeView, self).finalize_response(
            request, response, *args, **kwargs)
        for serialized_type in ('csv', 'json', 'yaml', 'xml', 'pdf', 'html'):
            if response.accepted_renderer.format == serialized_type:
                response['content-disposition'] = \
                    "filename={}.{}".format(self.filename, serialized_type)
        return response
