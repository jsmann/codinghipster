import codecs
import os

from django import template
from codinghipster.settings import STATIC_ROOT

register = template.Library()

@register.simple_tag
def load_css(obj):
    value = os.path.join(STATIC_ROOT, obj)
    with codecs.open(value, encoding='utf8') as fd:
        data = u''.join(fd.readlines())
    return u"<style>\n{}\n</style>".format(data)
