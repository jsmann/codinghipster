from django.contrib import admin
from . import models


class ResumeAdmin(admin.ModelAdmin):
    pass


class InfoAdmin(admin.ModelAdmin):
    pass


class JobAdmin(admin.ModelAdmin):
    pass


class ResponsibilityAdmin(admin.ModelAdmin):
    pass


class SkillsetAdmin(admin.ModelAdmin):
    pass


class SkillAdmin(admin.ModelAdmin):
    pass


class EducationAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Resume, ResumeAdmin)
admin.site.register(models.Info, InfoAdmin)
admin.site.register(models.Job, JobAdmin)
admin.site.register(models.Responsibility, ResponsibilityAdmin)
admin.site.register(models.Skillset, SkillsetAdmin)
admin.site.register(models.Skill, SkillAdmin)
admin.site.register(models.Education, EducationAdmin)
