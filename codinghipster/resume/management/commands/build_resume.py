from django.core.management.base import BaseCommand
from resume.models import (
    Resume, Info, Interest, Job, Responsibility, Achievement, Skillset, Skill,
    Education
)


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        resume = Resume()
        resume.save()

        info = Info(
            name='Joseph S Mann', email='hiring@codinghipster.com',
            address='85 Dexter Ave.', city='Watertown', state='Ma',
            zipcode='02472', phone_number='617-600-8480', resume=resume
        )
        info.save()

        for importance, description in enumerate((
                'Agile Development',
                'Domain Driven Design',
                'RESTful APIs',
                'Service Oriented Architecture',
                'Microservices',
                'Cloud Computing & Storage',
                'Open Source',
                )):
            Interest(
                importance=importance, description=description, resume=resume
            ).save()

        job = Job(
            title='Embedded Systems Intern', company='Emulex',
            location='Bolton, Ma', start_date='2007-05-01',
            end_date='2008-01-01', summary=(
                'Summer internship working with firmware team helping with '
                'team operations and maintaining team test and debug tools'),
            resume=resume
        )
        job.save()

        for importance, description in enumerate((
                'Maintained Nightly Regression Suite (Perl)',
                'Assist development team developing and maintaining testing '
                'tools'
                )):
            Responsibility(
                importance=importance, description=description,
                job=job).save()

        for importance, description in enumerate((
                'Extended firmware download utility to work with prototype '
                'hardware (C++)',
                )):
            Achievement(
                importance=importance, description=description,
                job=job).save()


        job = Job(
            title='Software Engineering Intern', company='NVIDIA',
            location='Santa Clara, Ca', start_date='2007-11-01',
            end_date='2008-02-01', summary=(
                'Winter Internship / Final Undergraduate project working '
                'for Linux storage firmware/driver team extending test and '
                'debug tools'
            ),
            resume=resume
        )
        job.save()

        for importance, description in enumerate((
                )):
            Responsibility(
                importance=importance, description=description,
                job=job).save()

        for importance, description in enumerate((
                'Implemented Windows GUI to interface with cli and WinDBG '
                'extension (C#)',
                'Implemented cli interface for low level memory access API '
                '(Perl)',
                )):
            Achievement(
                importance=importance, description=description,
                job=job).save()


        job = Job(
            title='Senior Software Engineer', company='Emulex',
            location='Bolton, Ma', start_date='2008-05-01',
            end_date='2013-01-01', summary=(
                'Started with Linux QA team testing full product stack '
                'quickly transitioned to development team maintaining test '
                'and build tools, as well as architecting new solutions. '
                'Eventually fixing Linux kernel driver and library bugs'
            ),
            resume=resume
        )
        job.save()

        for importance, description in enumerate((
                'Managed intern team used to support internal testing '
                'framework',
                'Maintained nightly testing and CI infrastructure (Perl)',
                'Fixed kernel Linux driver and library userspace interface '
                'bugs (C)',
                'Interfaced with major Linux distributions for inbox driver '
                'submissions and bug fixes',
                'Handled driver release responsibilities and maintained '
                'python build scripts',
                'Maintained svn branching structure and merges',
                'QAed Linux driver, interface library and GUI '
                'configuration tool'
                'Helped write regression tests'
                )):
            Responsibility(
                importance=importance, description=description,
                job=job).save()

        for importance, description in enumerate((
                'Reverse engineered RPM binary driver build process in order '
                'to extend support for other distributions',
                'Rewrote internal testing framework to be easier to maintain '
                'and extend (Python)',
                )):
            Achievement(
                importance=importance, description=description,
                job=job).save()


        job = Job(
            title='Software Engineer', company='Nasuni',
            location='Natick, Ma', start_date='2013-02-01',
            end_date='2014-12-01', summary=(
                'Started with automation team maintaining automation '
                'framework and building messaging queue. Switched to '
                'development team working on core product, fixing bugs '
                'and building new solutions for enterprise storage using '
                'cloud storage and other technologies'
            ),
            resume=resume
        )
        job.save()

        for importance, description in enumerate((
                'Maintained UI (Python/CSS/JS) and core libraries '
                '(Python/C/C++) of the Nasuni Storage Filer and Console',
                'Maintained testing infrastructure and automation dashboard '
                '(python)',
                )):
            Responsibility(
                importance=importance, description=description,
                job=job).save()

        for importance, description in enumerate((
                'Led feature team adding FTP mounting ability and other '
                'related features to UI and core libraries',
                'Helped implement "perfect file locking" between filers '
                'to create an effective distributed file system',
                'Add feature versioning for maintaining compatibility with '
                'older version of the filer',
                'Designed "sideloading" feature for installing new versions '
                'of the filer'
                )):
            Achievement(
                importance=importance, description=description,
                job=job).save()


        job = Job(
            title='Senior Software Engineer', company='Buildium',
            location='Boston, Ma', start_date='2015-01-01', summary=(
                'Work with Agile development team on property management '
                'software designed to help property managers. Architect '
                'backend solutions and participate in design sessions to '
                'deliver products that meet customers need'
            ),
            resume=resume
        )
        job.save()

        for importance, description in enumerate((
                'Develop property management software for residential '
                'property owners and management companies',
                'Work with .net (C#) and mysql to implement features as part '
                'of the core Buildium product',
                'Follow Agile methods using Scrum to deliver solutions on '
                'schedule',
                )):
            Responsibility(
                importance=importance, description=description,
                job=job).save()

        for importance, description in enumerate((
                "Brought on to team as an 'agent of change' to improve team's "
                'Agile practices and make the team more reliable and '
                'effective',
                'Extended legacy billing system to support new types of add-'
                'on services',
                'Helped integrate Buildium residential lease tracking with '
                'Assurant Renters Insurance offering',
                'Helped add tenant background check and credit checking '
                'service'
                )):
            Achievement(
                importance=importance, description=description,
                job=job).save()


        skillset = Skillset(name='Programming Languages', resume=resume)
        skillset.save()
        Skill(importance=1, value='Python', skillset=skillset).save()
        Skill(importance=2, value='C#', skillset=skillset).save()
        Skill(importance=3, value='Golang', skillset=skillset).save()
        Skill(importance=4, value='C', skillset=skillset).save()
        Skill(importance=5, value='C++', skillset=skillset).save()
        Skill(importance=6, value='Bash', skillset=skillset).save()
        Skill(importance=7, value='Perl', skillset=skillset).save()
        Skill(importance=8, value='Javascript', skillset=skillset).save()

        skillset = Skillset(name='Web Frameworks and Tools', resume=resume)
        skillset.save()
        Skill(importance=1, value='ASP.NET', skillset=skillset).save()
        Skill(importance=2, value='Django', skillset=skillset).save()
        Skill(importance=3, value='Flask', skillset=skillset).save()
        Skill(importance=4, value='AngularJS', skillset=skillset).save()
        Skill(importance=4, value='NodeJS', skillset=skillset).save()

        skillset = Skillset(name='Databases and Object Storage', resume=resume)
        skillset.save()
        Skill(importance=1, value='MySQL', skillset=skillset).save()
        Skill(importance=2, value='Postgres', skillset=skillset).save()
        Skill(importance=3, value='MongoDB', skillset=skillset).save()
        Skill(importance=4, value='Memcached', skillset=skillset).save()

        skillset = Skillset(
            name='Other Technologies and Utilities', resume=resume
        )
        skillset.save()
        Skill(importance=1, value='Selenium', skillset=skillset).save()
        Skill(importance=2, value='Jenkins', skillset=skillset).save()
        Skill(importance=3, value='RESTful APIs', skillset=skillset).save()

        education = Education(
            school='WPI', location='Worcester, Ma', start_date='2004-08-01',
            end_date='2008-05-01', degree='Bachelor of Science',
            concentration='Computer Science', resume=resume
        )
        education.save()
